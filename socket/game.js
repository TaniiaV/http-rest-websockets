import { getCurrentUser, userLeave, getRoomUsers } from "../utils/users.mjs";
import { createRoom, getRooms, addUserToRoom } from "../utils/rooms.mjs";

const rooms = getRooms();

export default io => {
    io.on("connection", socket => {
        console.log('room connection');

        io.emit('DISPLAYS_ROOMS', rooms);

        socket.on("CREATE_ROOM", roomName => {
            const room = createRoom(socket.id, roomName);

            if (room !== null) {
                socket.emit('CREATE_ROOM_NAME', roomName);
                io.emit('DISPLAYS_ROOMS', rooms);
            } else {
                socket.emit('ROOM_NAME_EXISTS', roomName + ' exists.');
            }
        });

        socket.on('JOIN_ROOM', ({ username, roomName }) => {
            const user = getCurrentUser(username);
            const room = addUserToRoom(user, roomName);
            socket.join({ username, room });

        });

        socket.on('disconnect', () => {
            console.log('room disconnection');
        });

    });
}