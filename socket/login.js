import { createUser } from "../utils/users.mjs";
let connections = [];

export default io => {
  io.on('connection', socket => {
    console.log('connection successful');
    connections.push(socket);

    socket.on('CREATE_USER', data => {
      const user = createUser(socket.id, data);

      if (user !== null) {
        socket.emit('CREATE_USER_DONE', { username: data });
      } else {
        socket.emit('USER_NAME_EXISTS', data + ' username is taken! Try some other username.');
      }
    });

    socket.on('disconnect', data => {
      console.log('connection unsuccessful');
      connections.splice(connections.indexOf, 1)
    });
  });
}

