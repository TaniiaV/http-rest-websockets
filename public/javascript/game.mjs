import { createElement, addClass, removeClass } from "./helper.mjs";
const socket = io("http://localhost:3002/game");

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const createRoomBtn = document.getElementById('create-room');
const backRoomBtn = document.getElementById('back-room');
const readyBtn = document.querySelector('.ready-btn');


const onClickCreateRoomBtn = () => {
  let roomName = prompt('Enter room name', 'my room');
  let isRoomExists = false;

  socket.emit("CREATE_ROOM", roomName);
  socket.on("ROOM_NAME_EXISTS", roomName => {
    alert(roomName);
    removeClass(document.querySelector('#rooms-page'), 'display-none');
    addClass(document.querySelector('#game-page'), 'display-none');
    isRoomExists = true;
  });

  if (!isRoomExists) {
    createRoom(roomName, username);
  }
}

const onClickBackRoomBtn = () => {
  removeClass(document.querySelector('#rooms-page'), 'display-none');
  addClass(document.querySelector('#game-page'), 'display-none');
}

backRoomBtn.addEventListener("click", onClickBackRoomBtn);

function createRoom(roomTitle, userName) {
  removeClass(document.querySelector('#game-page'), 'display-none');
  addClass(document.querySelector('#rooms-page'), 'display-none');
  document.querySelector('.room-name').innerText = roomTitle;
  document.querySelector('.active-user .small-title').innerText = userName;
}

createRoomBtn.addEventListener("click", onClickCreateRoomBtn);

function showRooms(rooms) {
  const keys = Object.keys(rooms);

  if (keys.length !== 0) {
    const roomsWrapper = document.querySelector('.rooms-wrapper');

    keys.forEach(key => {
      let idKey = key.split('').splice(6).join('');
      roomsWrapper.innerHTML += `<div class="room-block">
          <span class="small-title"><span class="user-counter"></span> user-connected</span>
          <div class="title">${rooms[key].name}</div>
          <button id="${idKey}" class="join-btn">Join</button>
        </div>`;
      document.getElementById(`${idKey}`).dataset.name = rooms[key].name;
    });
  }
  const allRooms = document.querySelectorAll('.room-block');
  allRooms.forEach(item => {
    item.addEventListener("click", (event) => {
      if (event.target.dataset.name) {
        onClickJoinBtn(event.target.dataset.name);
      }
    });
  });
}

socket.on("DISPLAYS_ROOMS", showRooms);

function onClickJoinBtn(roomName) {
  removeClass(document.querySelector('#game-page'), 'display-none');
  addClass(document.querySelector('#rooms-page'), 'display-none');
  socket.on('JOIN_ROOM', ({ username, roomName }));
  outputRoomName(roomName);
}

function outputRoomName(roomName) {
  document.querySelector('.room-name').textContent = roomName;
}

function outputUsers(users) {
  const userList = document.querySelector('.users');

  userList.innerHTML = `${users.map(user =>
    `<div class="active-user">
        <div class="user-title">
          <div class="circle-red"></div>
          <span class="small-title">${user.username}</span>
        </div>
        <div class="progress-bar"></div>
      </div>`).join('')}`;
}

function onClickReadyBtn() {
  removeClass(document.querySelector('.not-ready-btn'), 'display-none');
  addClass(document.querySelector('.ready-btn'), 'display-none');
}

readyBtn.addEventListener("click", onClickReadyBtn);




