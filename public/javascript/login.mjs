const username = sessionStorage.getItem("username");

if (username) {
  window.location.replace("/game");
}

const socket = io("http://localhost:3002/login");

const submitButton = document.getElementById("submit-button");
const input = document.getElementById("username-input");

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
  const inputValue = getInputValue();
  if (!inputValue) {
    return;
  }
  sessionStorage.setItem("username", inputValue);
  socket.emit('CREATE_USER', inputValue);
  socket.on('USER_NAME_EXISTS', data => {

    alert(data);
    sessionStorage.clear();
  });

  const newUsername = sessionStorage.getItem("username");

  if (newUsername) {
    window.location.replace("/game");
  } else {
    window.location.replace("/login");

  }
};

const onKeyUp = ev => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};

submitButton.addEventListener("click", onClickSubmitButton);
window.addEventListener("keyup", onKeyUp);

function setUsername() {
  socket.emit('setUsername', inputValue);
};


