const rooms = {};

export function createRoom(id, roomName) {
    const keys = Object.keys(rooms);

    if (keys) {
        const existRoomName = keys.find(key => rooms[key].name === roomName);

        if (existRoomName) {
            return null;
        } else {
            rooms[id] = {
                name: roomName,
                users: []
            }
            return rooms[id];
        }
    }
}

export function userJoin(user, roomId) {
    if (rooms.hasOwnProperty(roomId)) {
        rooms[roomId].users.push(user);
    }

    return rooms[roomId].users;
}

export function getCurrentRoom(id) {
    return rooms[id];
}

export function addUserToRoom(user, roomName) {
    const keys = Object.keys(rooms);

    if (keys && user && roomName) {
        return keys.find(key => {
            if (rooms[key].name === roomName) {
                rooms[key].users.push(user);
                console.log({ key, users: rooms[key].users })
                console.log(user, roomName)

                return { key, users: rooms[key].users };
            }
        });
    }
}

export function userLeave(userId, roomId) {
    const index = rooms[roomId].users.findIndex(user => user.id === userId);

    if (index !== -1) {
        rooms[roomId].users.splice(index, 1);
    }

    return rooms[roomId].users;
}

export function getRooms() {
    return rooms;
}


