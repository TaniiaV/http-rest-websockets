const users = [];

export function createUser(id, username) {

  if (users.find(item => item.username == username) === undefined) {
    const user = { id, username };
    users.push(user);
    return user;
  } else {
    return null;
  }

}

export function getCurrentUser(username) {
  return users.find(user => user.username === username);
}



